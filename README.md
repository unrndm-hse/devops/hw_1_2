Git Orphaned
============

Deadline
========
`-`

Task
====

Imagine a situation: you told your colleagues that master-branch is prohibited for pushing, Instead, you should create and push commits to the dev branch, and then update the code by creating pull requests to master.


Now you have to do a code review. After entering the git repo you have noticed that all code has been committed to master, and you cannot review the code.


You need to bring all code to its place, and all commits should remain in history!

In this task you have to create a repo with several commits to master. Then you need to explain your commands/steps, that bring repo to the desired state.
